<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'martinemalterre');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'V^!}p|V$|CIxuz7nQ=`,KQ,tW&W|$rQ8!OK<%u`*-D;P51st=jMemqN]YMhjvGe!');
define('SECURE_AUTH_KEY',  ']JV<AZPl;wC54?f)2fRc?NPm`L)/C`wDD}=/is;mYE9Jhf|{,x9jzZYNvXCsA4:7');
define('LOGGED_IN_KEY',    'F<oFRhlNm~LaCfOT$25VidNfn8qq++M#L@O2s*rMVO3BFlP..=@-5vU-0TRx[#NY');
define('NONCE_KEY',        '!MO[?nu? <c{dvWwUx{bTV<5T/#v{LDe wWwrS%14GKn4Wy1k{-ZW5L9yP4-JF/y');
define('AUTH_SALT',        'C14_Tl@gR$T|xnl=<83?} Dyyy^}pcJFZ?/uZCf>5J[(1=vB}hm{]=#GU_2]VG$~');
define('SECURE_AUTH_SALT', '#d.F{,EYH96#MlTM`cQkbL/<iK-~U$_K88)%!BJSl]}A[nT[cRM9s<gZ _kX_&PG');
define('LOGGED_IN_SALT',   'x_d(F1}q/ca*AzlW&YK}<d,uG?pFzgi7cDMhE]}BWx$zk].lO6e=xj*KZH+y3~Zt');
define('NONCE_SALT',       ')R7O}#>w#Xxh(Vj9&X5_s8d|nFdaKIWalP+>3.B.`gNxG20bl*42{:j/ *1wegs}');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');